%include 'lib.inc'

global find_word


find_word:
	.loop:
		test rsi, rsi
		jz .not_found
		push rsi
		push rdi
		add rsi, 8
		call string_equals
		pop rdi
		pop rsi
		test rax, rax
		jnz .found
		mov rsi, [rsi]
		jmp .loop
	.not_found:
    		mov rdi, 0
    		ret
	.found:
		mov rdi, rsi
		add rdi, 8
		push rdi
		call string_length
		pop rdi
		add rdi, rax
		inc rdi
		ret

