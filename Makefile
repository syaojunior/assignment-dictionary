ASM=nasm
ASMFLAGS=-f elf64
LD=ld

all: lab2

dict.o: dict.asm lib.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

lab2: main.o dict.o
	$(LD) -o $@ $^

main.o: main.asm dict.o *.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

clean:
	rm -f *.o dictionary

.PHONY: clean
