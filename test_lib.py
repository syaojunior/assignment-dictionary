import subprocess

count = 0


def make():
    subprocess.run(["make"])


def test(input, output="", error=False):
    global count
    program = subprocess.run(["./lab2"], input=input, capture_output=True, text=True)
    # print("stdout:", program.stdout)
    # print("stderr:", program.stderr)
    out = program.stdout
    if error:
        out = program.stderr
    if out != output + "\n":
        raise Exception(out, "!=", output)
    count += 1
    print("Test", count, ": Accepted [", input, ",", output, "]")


def test_dict_items(dict):
    for i in dict.items():
        test(i[0], i[1])


def create_words(dict):
    with open("./words.inc", "w") as file:
        file.write("%include \"colon.inc\"\n")
        index = 0
        for i in dict.items():
            file.write("colon \"" + i[0] + "\", word" + str(len(dict) - index) + "\n")
            file.write("db \"" + i[1] + "\", 0\n")
            index += 1
        file.write("\n")

def test_dict(dict):
    create_words(dict)
    make()
    test_dict_items(dict)

dict = {
    "word1": "word1",
    "word2": "word2",
    "word3": "word3",
    "word4": "word4",
    "word5": "word4"
}

# Main



test_dict(dict)
# test("Java", "Spring")
test("1231e21e", "Not Found", error=True)
