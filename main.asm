%include 'lib.inc'
%include 'words.inc'

%define buffer_size 255

section .data
	buffer: times buffer_size db 0x00
section .rodata
	error: db "Not Found", 10, 0
	input_too_long: db "Key entered is too long", 0



global _start
extern find_word

section .text



_start:
    ; Получаем слово
	mov rdi, buffer
	mov rsi, buffer_size
	call read_word

	cmp rax, 0
    jz .too_long

    ; Ищем слово
	mov rdi, buffer
	mov rsi, word1
	call find_word
	; if
	test rdi,rdi
	jnz .found
    ; Если слово не найдено
	.not_found:
	    ; Печатаем ошибку
	    mov rdi, error
    	call print_string_err
    	mov rdi, 1
    	call exit
    ; Если слово найдено
    .found:
    	; Печатаем результат
        push rdi
        call print_string
        call print_newline
        mov rdi, 0
        call exit
    .too_long:
	    mov rdi, input_too_long
    	call print_string_err
    	mov rdi, 0
    	call exit


